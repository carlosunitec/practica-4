package mx.unitec.practica4

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_login.*

const val Extra_RFC = "mx.unitec.practica4.RFC"

class LoginActivity : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        if(DataPreferences.getStoredRFC(this) !=""){
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    fun clickRFC(view: View){
        //validar editTextRFC
        val intent = Intent(this, MainActivity::class.java).apply {
            putExtra(Extra_RFC, txtRFC.text.toString())
        }

        startActivity(intent)
        finish()

    }

}